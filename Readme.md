# app-listen

```js
var express = require('express');
var app = express();

// this
require('app-listen')(app);

// is basically equivalent to

app.listen(app.get('port'), '127.0.0.1', function() {
  console.log('app listening on port %d in %s mode', app.get('port'), app.get('env'));
});
```
