module.exports = function(app, host) {
  var port = app.get('port');
  if (!port) throw new Error('please set a port using app.set(\'port\', process.env.PORT||3000);');
  app.listen(port, host||'127.0.0.1', function() {
    console.log('app listening on %d in %s mode', app.get('port'), app.get('env'));
  });
};
